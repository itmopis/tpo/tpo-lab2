package ru.kastricyn.tpo2

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.function.BiFunction
import kotlin.math.PI

class MyMath {
    companion object {
        fun bigDecimal(i: Long) =
            BigDecimal.valueOf(i).setScale(20, RoundingMode.HALF_EVEN)

        fun bigDecimal(i: Double) =
            BigDecimal.valueOf(i).setScale(20, RoundingMode.HALF_EVEN)
    }

    private fun lbinsearch(lstart: Int, rstart: Int, check: (Int) -> Boolean): Int {
        var (l, r) = intArrayOf(lstart, rstart)
        while (l < r) {
            val m = l + r shr 1
            if (check(m))
                r = m
            else
                l = m + 1
        }
        return l
    }

    private fun factorial(n: Int): BigDecimal {
        if (n < 0) throw IllegalArgumentException("n must be >= 0: n = $n not >= 0")
        var result = bigDecimal(1)
        for (i in 2L..n) result *= bigDecimal(i)
        return result
    }

    private fun abstractFunction(
        x: BigDecimal,
        epsilon: BigDecimal = bigDecimal(0.01),
        partOfTaylor: BiFunction<Int, BigDecimal, BigDecimal>
    ): BigDecimal {
        val n = lbinsearch(1, 1000) { partOfTaylor.apply(it, x).abs() < epsilon }
        return (0..n).map { partOfTaylor.apply(it, x) }.fold(bigDecimal(0)) { a, b -> a + b }
    }


    fun cos(x: BigDecimal, epsilon: BigDecimal = bigDecimal(0.01)): BigDecimal =
        abstractFunction(x, epsilon)
        { n, x -> bigDecimal(-1).pow(n) * x.pow(2 * n) / factorial(2 * n) }

    fun sin(x: BigDecimal, epsilon: BigDecimal = bigDecimal(0.01)): BigDecimal =
        -cos(x + bigDecimal(PI / 2), epsilon)

    fun tan(x: BigDecimal, epsilon: BigDecimal = bigDecimal(0.01)): BigDecimal =
        sin(x, epsilon) / cos(x, epsilon)

    fun sec(x: BigDecimal, epsilon: BigDecimal = bigDecimal(0.01)): BigDecimal =
        bigDecimal(1) / cos(x, epsilon)

    fun ln(x: BigDecimal, epsilon: BigDecimal = bigDecimal(0.01)): BigDecimal =
        abstractFunction(x, epsilon)
        { m, a ->
            val x = a - bigDecimal(1L);
            val n = m + 1;
            bigDecimal(-1).pow(n - 1) * x.pow(n) / bigDecimal(n.toLong())
        }

    private fun log(base: BigDecimal, x: BigDecimal, epsilon: BigDecimal = bigDecimal(0.01)): BigDecimal =
        ln(x, epsilon) / ln(base, epsilon)

    fun lg(x: BigDecimal, epsilon: BigDecimal = bigDecimal(0.01)): BigDecimal =
        log(bigDecimal(10L), x, epsilon)

    fun log2(x: BigDecimal, epsilon: BigDecimal = bigDecimal(0.01)): BigDecimal =
        log(bigDecimal(2L), x, epsilon)


    fun testModule(arg: BigDecimal, precision: BigDecimal = bigDecimal(0.01)): Double {
        val eps = precision * bigDecimal(0.01)
        val x = arg
        return if (arg <= bigDecimal(0L))
            (tan(x, eps) * sec(x, eps)).toDouble()
        else {
            (((((lg(x) / lg(x)) * ln(x)) - (log2(x).pow(3))) + ln(x)).pow(3)).toDouble()
        }
    }
}