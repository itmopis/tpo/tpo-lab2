import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.SpyK
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvFileSource
import ru.kastricyn.tpo2.MyMath
import ru.kastricyn.tpo2.MyMath.Companion.bigDecimal
import java.io.IOException
import java.io.PrintWriter
import java.math.BigDecimal
import java.nio.file.Files
import java.nio.file.Paths
import java.util.Scanner
import kotlin.math.cos
import kotlin.math.ln
import kotlin.math.log10
import kotlin.math.log2
import kotlin.math.sin
import kotlin.math.tan

class Test {
    @SpyK
    private var idealMath = MyMath()

    @SpyK
    private var mySinAndLn = MyMath()

    @SpyK
    private var mySinCosAndLnLg = MyMath()

    @SpyK
    private var allMy = MyMath()
    private lateinit var testInfo: TestInfo

    @BeforeEach
    fun clearMockSetting(testInfo: TestInfo): Unit {
        MockKAnnotations.init(this)
        this.testInfo = testInfo
    }

    @ParameterizedTest(name = "idealTestPrecision={3}")
    @CsvFileSource(resources = ["sourceDefault.csv"])
    fun idealTest(l: Double, r: Double, step: Double, precision: Double) {
        every { idealMath.cos(any(), any()) } answers { bigDecimal(cos(firstArg<BigDecimal>().toDouble())) }
        every { idealMath.sin(any(), any()) } answers { bigDecimal(sin(firstArg<BigDecimal>().toDouble())) }
        every { idealMath.sec(any(), any()) } answers { bigDecimal(1 / cos(firstArg<BigDecimal>().toDouble())) }
        every { idealMath.tan(any(), any()) } answers { bigDecimal(tan(firstArg<BigDecimal>().toDouble())) }
        every { idealMath.ln(any(), any()) } answers { bigDecimal(ln(firstArg<BigDecimal>().toDouble())) }
        every { idealMath.lg(any(), any()) } answers { bigDecimal(log10(firstArg<BigDecimal>().toDouble())) }
        every { idealMath.log2(any(), any()) } answers { bigDecimal(log2(firstArg<BigDecimal>().toDouble())) }
        writeTests(
            "${testInfo.displayName}.csv",
            idealMath,
            bigDecimal(l),
            bigDecimal(r),
            bigDecimal(step),
            bigDecimal(precision)
        )
    }

    @ParameterizedTest(name = "mySinAndLnTestPrecision={3}")
    @CsvFileSource(resources = ["sourceDefault.csv"])
    fun mySinAndLnTest(l: Double, r: Double, step: Double, precision: Double) {
        every { mySinAndLn.cos(any(), any()) } answers { bigDecimal(cos(firstArg<BigDecimal>().toDouble())) }
        every { mySinAndLn.sec(any(), any()) } answers { bigDecimal(1 / cos(firstArg<BigDecimal>().toDouble())) }
        every { mySinAndLn.tan(any(), any()) } answers { bigDecimal(tan(firstArg<BigDecimal>().toDouble())) }
        every { mySinAndLn.lg(any(), any()) } answers { bigDecimal(log10(firstArg<BigDecimal>().toDouble())) }
        every { mySinAndLn.log2(any(), any()) } answers { bigDecimal(log2(firstArg<BigDecimal>().toDouble())) }
        writeTests(
            "${testInfo.displayName}.csv",
            mySinAndLn,
            bigDecimal(l),
            bigDecimal(r),
            bigDecimal(step),
            bigDecimal(precision)
        )
    }

    @ParameterizedTest(name = "mySinCosAndLnLgTestPrecision={3}")
    @CsvFileSource(resources = ["sourceDefault.csv"])
    fun mySinCosAndLnLgTest(l: Double, r: Double, step: Double, precision: Double) {
        every { mySinCosAndLnLg.sec(any(), any()) } answers { bigDecimal(1 / cos(firstArg<BigDecimal>().toDouble())) }
        every { mySinCosAndLnLg.tan(any(), any()) } answers { bigDecimal(tan(firstArg<BigDecimal>().toDouble())) }
        writeTests(
            "${testInfo.displayName}.csv",
            mySinCosAndLnLg,
            bigDecimal(l),
            bigDecimal(r),
            bigDecimal(step),
            bigDecimal(precision)
        )
    }

    @ParameterizedTest(name = "allMyTestPrecision={3}")
    @CsvFileSource(resources = ["sourceDefault.csv"])
    fun allMyTest(l: Double, r: Double, step: Double, precision: Double) {
        writeTests(
            "${testInfo.displayName}.csv",
            allMy,
            bigDecimal(l),
            bigDecimal(r),
            bigDecimal(step),
            bigDecimal(precision)
        )
    }

    @Throws(IOException::class)
    fun writeTests(
        filename: String,
        module: MyMath,
        from: BigDecimal,
        to: BigDecimal,
        step: BigDecimal,
        precision: BigDecimal = bigDecimal(0.01)
    ) {
        val path = Paths.get("testResult", filename)
        Files.deleteIfExists(path)
        val file = Files.createFile(path)

        val printWriter = PrintWriter(file.toFile())
        var current = from
        printWriter.println("x, testModule(x)")
        while (current <= to) {
            try {
                printWriter.println("$current, ${module.testModule(current, precision)}")
            } catch (e: Exception) {
                printWriter.println("$current, $e")
            }
            current += step
        }
        printWriter.close()
    }

    companion object {

        @JvmStatic
        @BeforeAll
        fun outputFolder(): Unit {
            val names = arrayOf("testResult.old", "testResult").map(Paths::get)
            for (i in 0 until names.size - 1) {
                if (Files.isDirectory(names[i]))
                    FileUtils.deleteDirectory(names[i].toFile());
                if (Files.isDirectory(names[i + 1]))
                    Files.move(names[i + 1], names[i])
            }
            Files.createDirectory(names[names.size - 1])
        }

/*        @JvmStatic
//        @AfterAll
//        fun resultAll(): Unit {
//            val map = mutableMapOf<Pair<String, String>, String>()
//            val outputPath = Paths.get("testResult", "resultAll.csv")
//            Files.deleteIfExists(outputPath)
//            Files.walk(Paths.get("testResult")).forEach {f ->
//                Scanner(f.fileName).use {sc ->
//                    sc.nextLine()
//                    while (sc.hasNext()){
//
//                    }
//
//                }
//            }
//     } */
    }
}