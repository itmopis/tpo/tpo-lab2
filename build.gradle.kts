plugins {
    kotlin("jvm") version "1.8.0"
    jacoco
}

group = "ru.kastricyn"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
//    testImplementation(kotlin("test"))
    testImplementation("commons-io:commons-io:2.11.0")
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
    testImplementation("io.mockk:mockk:1.13.4")
}

tasks.test {
    useJUnitPlatform()
    systemProperty("junit.jupiter.execution.parallel.enabled", "true")
    systemProperty("junit.jupiter.execution.parallel.mode.default", "concurrent") // методы внутри класса
    systemProperty("junit.jupiter.execution.parallel.mode.classes.default", "concurrent") //сами классы
    finalizedBy(tasks.jacocoTestReport)
}


kotlin {
    jvmToolchain(17)
}
